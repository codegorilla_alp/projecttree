
<?php include('navbar.php'); ?>
<body>
    <div class="container">
    <h1 class="productlist-title" >Order List</h1>
    <div class="orderlist">
<?php
try {
    $conn = new PDO('mysql:host=127.0.0.1:8889;dbname=projectTREE', 'root', 'root');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->query("SELECT * FROM bestellingen ");
    while ($row = $stmt->fetch()) {
                //en het weergeven als pagina naar gebruiker als HTML
                echo '<li>' . 'ID ' . $row['id'] . ' | ';
                echo 'E-mail ' . $row['email'] . ' | ';
                echo 'Product ID ' . $row['productid'] . ' | ';
                echo '&euro; ' . $row['tebetalen'];
                echo '</li>';
            }
}

catch(PDOExeption $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

$conn = NULL;

?>

</div>
</div>
<?php include('footer.php'); ?>