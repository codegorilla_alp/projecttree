
<?php include('navbar.php'); ?>

<body>
    <div class="container">
        <h1 class="welcometext yellow">WELCOME TO MY <span class="orange">Fresh Fruit</span> webshop</h1>
       
        <h2 class="productlist-title">Just Arrived Fresh Fruit</h2>
        <div class="filterbox">
            <form>
                <li> <input type="text" name="naamfilter" placeholder="I am looking for..."></li>
                <li><button class="btn-search" type="submit" value="filter">Search</button></li>
                <li><button class="btn-search" type="submit" value="">Clear Search</button></li>
            </form>
        </div>
        <div class="productlist">
        <?php

if (isset($_GET['naamfilter'])){
    $naamfilter = $_GET['naamfilter'];
}
else {
    $naamfilter = '';
}


try {
    $conn = new PDO('mysql:host=127.0.0.1:8889;dbname=projectTREE', 'root', 'root');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->query("SELECT * FROM producten WHERE naam LIKE '%$naamfilter%' ");
    while ($row = $stmt->fetch()) {
                //en het weergeven als pagina naar gebruiker als HTML
                echo '<li>' . $row['naam'] . '  € ' . $row['prijs'];
                echo '<a class="btn-update" href="koopproduct.php?productid=' . $row['id'] . '">Buy</a>';
                echo '<a class="btn-change" href="productbewerken.php?productid=' . $row['id'] . '">Change</a>';
                echo '<a class="btn-delete" href="dbproductenverwijderen.php?productid=' . $row['id'] . '">Remove</a>';
                echo '</li>';
            }
}

catch(PDOExeption $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

$conn = NULL;

?>
    </div>
        
    </div>

    <?php include('footer.php'); ?>
