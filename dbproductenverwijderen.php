<?php

$productid = $_GET['productid'];

try {
    $conn = new PDO('mysql:host=127.0.0.1:8889;dbname=projectTREE', 'root', 'root');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare('DELETE from producten WHERE ID=:fid');

    $stmt->execute([
        'fid' => $productid
    ]);
}

catch(PDOExeption $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

$conn = NULL;

header ('Location: index.php' );

?>