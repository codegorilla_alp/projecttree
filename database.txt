MySQL starten via Terminal
mysql -u root -h 127.0.0.1


create database projectTREE;



use projectTREE; 



create table producten (
	id int not null auto_increment PRIMARY KEY,
	naam varchar (255) not null,
	prijs DECIMAL(5,2) not null
);


INSERT INTO producten (naam, prijs) VALUES ('1 KG Apple', 2.95);


MySQL starten via Terminal
mysql -u root -h 127.0.0.1


create database projectTREE;



use projectTREE; 



create table producten (
	id int not null auto_increment PRIMARY KEY,
	naam varchar (255) not null,
	prijs DECIMAL(5,2) not null
);


INSERT INTO producten (naam, prijs) VALUES ('1 KG Apple', 2.95);



CREATE TABLE bestellingen (
	id int not null auto_increment PRIMARY KEY,
	email varchar(255) NOT NULL,
	productid INT,
	FOREIGN KEY (productid) REFERENCES producten(id),
	tebetalen DECIMAL(10,2) NOT NULL
);



insert into bestellingen (email, productid, tebetalen) VALUES ('richard_alp@hotmail.com', 10, 55.00);

INSERT INTO bestellingen (email, productid, debetalen) VALUES ('richard_alp@hotmail.com', 20,22.00);

delete from bestellingen where id=1;

