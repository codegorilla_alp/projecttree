<?php include('navbar.php'); ?>
<?php
    include_once("dblib.php");

    $productid = $_GET['productid'];
    $productnaam = dblookup("producten", "naam", $productid);
    $productprijs = dblookup("producten", "prijs", $productid);

?>
<body>
    <div class="container">
    <h1 class="productlist-title">Change Product</h1>
    <div class="filterbox">
        <form action="dbproductupdate.php" method="POST">
        <li><input type="text" name="productid" placeholder="Product Id" value="<?php echo $productid ?>"></li>
        <li><input type="text" name="productnaam" placeholder="Fruit Name" value="<?php echo $productnaam ?>"></li>
        <li><input type="text" name="productprijs" placeholder="Price" value="<?php echo $productprijs ?>"></li>
        <li><button class="btn-search" type="submit">Update fruit</button></li>
    </form>
    </div>
    </div>
<?php include('footer.php'); ?>


