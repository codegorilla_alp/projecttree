<head>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<header>
    <div class="container">
        <h1>Fresh Fruit Webshop</h1>
        <nav>
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="producttoevoegen.php">Add Fruit</a></li>
                <li><a href="toonbestellingen.php">My Orders</a></li>
            </ul>
        </nav>
    </div>
</header>